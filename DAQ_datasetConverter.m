function [ outputfile, dataset ] = DAQ_datasetConverter( inputfile_name, recal_name, readConfig )
    
    %Convert a raw data file to something the DAN can digest;
    %   Doing various calibrations, resynchronizations etc.
    %   Arguments:
    %    inputfile_name  : the filename to convert.
    %    recal_name      : Optional: string that get appended to the end of the new filename
    %                      (with a _ prepended). NaN to ignore.
    %    readConfig      : Optional: logical, true to re-read the CONFIG.m
    %                      and use the scope- and camera calibrations from there.
    %   
    %   Note that any ending '_raw' will be stripped off.
    %
    %   Returns:
    %    outputfile  : The filename of the outputfile as a char array.
    %    dataset     : The recalibrated dataset
    %   
    
    if nargin() == 0
       fprintf('Usage: DAQ_datasetConverter(''raw_datasetfile'', (recalibration_name), (readConfig) \n')
       return
    end
    
    % add utilities
    path = strrep(mfilename('fullpath'), mfilename, '');
    addpath([path 'utilities']);
    
    % Read the input data
    inputfile = load(inputfile_name);
    dataset = inputfile.dataset;
    
    savepath = dataset.metadata.savepath;
    filedir_proc = [savepath 'processed/' datestr(dataset.metadata.startTime, 'ddmmyyyy')];
    % make folder if not there
    if ~exist(filedir_proc,'dir')
        mkdir(filedir_proc)
    end
    
    % choose filename based on whether it is new or recalibrated
    if exist('recal_name','var') && ischar(recal_name)
        filename_proc = [filedir_proc '/' num2str(dataset.metadata.dataset_id) '_' recal_name '.mat'];
    elseif isnumeric(recal_name) && isnan(recal_name)
        filename_proc = [filedir_proc '/' num2str(dataset.metadata.dataset_id) '.mat'];
    else
        error('Bad recal_name, expected char array OR NaN')
    end
    
    %Re-load CONFIG for calibrations?
    config = NaN;
    if exist('readConfig','var') && readConfig == true
        config = CONFIG();
    end
    
    %If relevant, create the scalar metadata entry for GAUGE_X and GAUGE_Y
    if isfield(dataset.raw,'gaugeX') && ...
      ~isfield(dataset.scalars,'PLE_GAUGE_X')
        dataset.scalars.PLE_GAUGE_X.desc = 'PLE horizontal lens position gauge';
        dataset.scalars.PLE_GAUGE_Y.desc = 'PLE vertical lens position gauge';
        
        dataset.scalars.PLE_GAUGE_X.device = 'IP-TCP';
        dataset.scalars.PLE_GAUGE_Y.device = 'IP-TCP';
        
        dataset.scalars.PLE_GAUGE_X.unit = 'mm';
        dataset.scalars.PLE_GAUGE_Y.unit = 'mm';
    end

    % reset the shot counter
    dataset.metadata.numShots = 0;
    
    % calibrate & resynchronize the data to make a nice package for offline processing
    lostShots = dataset.metadata.lostShotsPerStep;
    shotsPerStep = dataset.metadata.shotsPerStep;
    numSteps1 = dataset.metadata.numSteps;
    numSteps2 = dataset.metadata.numSteps2;
    if ~isempty(dataset.metadata.camShotOffsets)
        % TODO: FIX IT
        offset_min = 0;
        offset_max = 0;
        for i = 1:numel(dataset.metadata.camShotOffsets)
            offset_min = min(dataset.metadata.camShotOffsets{i}, offset_min);
            offset_max = max(dataset.metadata.camShotOffsets{i}, offset_max);
        end
        %offset_min = min(min(dataset.metadata.camShotOffsets{:}), 0)
        %offset_max = max(max(dataset.metadata.camShotOffsets{:}), 0);
    else
        offset_min = 0;
        offset_max = 0;
    end
    for stepNum2 = 1:numSteps2
        for stepNum1 = 1:numSteps1
            for k = (1 - offset_min):(shotsPerStep + lostShots - offset_max)
                
                % generate synchronized index
                index_raw = k + (shotsPerStep + lostShots)*( (stepNum2-1)*numSteps1 + (stepNum1-1) );
                
                % stop saving in aborted runs
                if index_raw > dataset.metadata.numShots_raw - lostShots
                    break;
                end
                
                % add shot to processed dataset
                dataset = saveShotToDataset(dataset, index_raw, dataset.metadata.camShotOffsets, config);
            end
        end
    end

    % update the savepath
    dataset.metadata.savepath = filename_proc;

    % save to file
    save(filename_proc, 'dataset', '-v7.3');
    fprintf('Wrote processed dataset to file\n\t''%s''\n', filename_proc)
    
    outputfile = filename_proc;
    
end
