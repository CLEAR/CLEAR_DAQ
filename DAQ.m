function varargout = DAQ(varargin)
    
    % add utilities
    path = strrep(mfilename('fullpath'), ['/' mfilename], '/');
    addpath([path 'utilities']);
    
    % DAQ MATLAB code for DAQ.fig
    %      DAQ, by itself, creates a new DAQ or raises the existing
    %      singleton*.
    %
    %      H = DAQ returns the handle to a new DAQ or the handle to
    %      the existing singleton*.
    %
    %      DAQ('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in DAQ.M with the given input arguments.
    %
    %      DAQ('Property','Value',...) creates a new DAQ or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before DAQ_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to DAQ_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit theisScan2D above text to modify the response to help DAQ

    % Last Modified by GUIDE v2.5 07-Apr-2019 14:15:52

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @DAQ_OpeningFcn, ...
                       'gui_OutputFcn',  @DAQ_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


    % --- Executes just before DAQ is made visible.
    function DAQ_OpeningFcn(hObject, eventdata, handles, varargin)

        % to incaify MATLAB!
        try
            matlabJapc.staticINCAify('CTF');
            disp('INCAified');
        catch e
            if isempty(strfind(e.message, 'InCA client was already configured'))
                % display error if not just that it was already InCAified
                disp(e);
                disp(e.message);
            end
        end
        
        % get config
        config = CONFIG();
        
        % set experiment menu
        set(handles.exptMenu, 'String', cellstr(config.expts(:,2)));
        
        % set camera menu
        handles.camSelectionListbox.UserData = 1:size(config.cams,1);
        camStr = cell(1, size(config.cams,1));
        camLengths = zeros(1, size(config.cams,1));
        for i = 1:size(config.cams,1)
            camLengths(i) = numel(config.cams{i,2});
        end
        for i = 1:size(config.cams,1)
            %TODO: Fix for digicams (cosmetic)
            camStr{i} = [config.cams{i,2} repmat(' ', 1, max(camLengths)-camLengths(i)) ' (' strrep(config.cams{i,1}, '/Image', '') ')'];
        end
        set(handles.camSelectionListbox, 'String', camStr);
        
        % set scope menu
        handles.scopeSelectionListbox.UserData = 1:size(config.scopes,1);
        scopeStr = cell(1, size(config.scopes,1));
        scopeLengths = zeros(1, size(config.scopes,1));
        for i = 1:size(config.scopes,1)
            scopeLengths(i) = numel(config.scopes{i,2});
        end
        for i = 1:size(config.scopes,1)
            scopeDev = strsplit(config.scopes{i,1}, '/');
            scopeStr{i} = [config.scopes{i,2} repmat(' ', 1, max(scopeLengths)-scopeLengths(i)) ' (' scopeDev{1} ')'];
        end
        set(handles.scopeSelectionListbox, 'String', scopeStr);
        
        % set default background saving and elog printing
        set(handles.saveBgCheckbox, 'Value', config.defaults.saveBackground);
        set(handles.printToElogCheckbox, 'Value', config.defaults.printToElog);
        
        % set default scan direction
        switch config.defaults.scanDirection
            case 'forward'
                set(handles.forward1radiobutton, 'Value', 1);
                set(handles.forward2radiobutton, 'Value', 1);
            case 'backward'
                set(handles.backward1radiobutton, 'Value', 1);
                set(handles.backward2radiobutton, 'Value', 1);
            case 'both'
                set(handles.both1radiobutton, 'Value', 1);
                set(handles.both2radiobutton, 'Value', 1);
            case 'random'
                set(handles.random1radiobutton, 'Value', 1);
                set(handles.random2radiobutton, 'Value', 1);
        end
        
        % make scan parameter menus
        set(handles.param1menu, 'String', cellstr(config.scans(:,1)));
        set(handles.param2menu, 'String', cellstr(config.scans(:,1)));
        updateTimeEstimate(handles);
        
        % Make numPoints menus
        set(handles.numPoints1menu, 'String', cellstr({'Number of points', 'Step size'}));
        set(handles.numPoints2menu, 'String', cellstr({'Number of points', 'Step size'}));
        
        % Update handles structure
        guidata(hObject, handles);

        % update steps
        param1menu_Callback(handles.param1menu, eventdata, handles)
        param2menu_Callback(handles.param2menu, eventdata, handles)
        
        % Select default scopes and cameras
        changeExperiment(1, handles)

    % stops all monitors when the window is closed
    function figure1_CloseRequestFcn(hObject, eventdata, handles)  %#ok<DEFNU>
        
        % stop and delete the monitor
        if isfield(handles, 'shotMonitor')
            stop(handles.shotMonitor);
            delete(handles.shotMonitor);
        end
        
        % delete the hObject
        if exist('hObject','var')
            delete(hObject);
        end
        
        
    % --- Outputs from this function are returned to the command line.
    function varargout = DAQ_OutputFcn(hObject, eventdata, handles)  %#ok<DEFNU>

    
    
    %% AQCUISITION OF A DATASET
    
    function acquireButton_Callback(hObject, eventdata, handles)  %#ok<DEFNU>
        config = CONFIG();
        
        % disable the button and enable aborts
        set(hObject,'Enable','off');
        set(handles.abortButton,'Enable','on');
        
        % set up the dataset based on settings
        dataset = struct();
        
        % harvest the metadata from the GUI and save
        MD = harvestMetadata(handles);
        dataset.metadata = MD;
        
        %Connect to gauge server arduinos
        handles.positionGaugeConnectors = {}; %name, handleX, handleY
        for i = 1:size(config.posgauges,1)
            handles.positionGaugeConnectors{i,1} = config.posgauges{i,1};
            handles.positionGaugeConnectors{i,2} = positionGaugeConnector(config.posgauges{i,2}, config.posgauges{i,3});
            handles.positionGaugeConnectors{i,3} = positionGaugeConnector(config.posgauges{i,2}, config.posgauges{i,4});
            
            %handles.dataset.positionGauge
        end
        
        % acquire the machine state
        dataset.state = harvestMachineState();
        
        % set up the data structure
        [scalars, scopes, images] = setupDatasetStructure(MD);
        dataset.scalars = scalars;
        dataset.scopes = scopes;
        dataset.images = images;
        
        % TODO: take background
        
        % prepare the raw dataset
        dataset.raw = struct();
        dataset.raw.BSA = {};
        dataset.raw.settings = {};
        
        % attach dataset to handles
        handles.dataset = dataset;
        
        % prepare progress display
        setupStepProgressText(handles, MD);
        
        % set up the DAQ progress values
        progress = struct();
        progress.shots_total = 1;
        progress.shots_in_step = 1;
        progress.shots_raw_total = 1;
        progress.shots_raw_in_step = 1;
        progress.step_num1 = 1;
        progress.step_num2 = 1;
        handles.progress = progress;
        
        % setup the monitor and harvester
        % Note: Digital cameras must be monitored with emtpy selector,
        % whereas other cameras needs a non-empty selector...
        shotMonitor = matlabJapcMonitor('SCT.USER.ALL', ...
                                        [dataset.metadata.scopeDevices; dataset.metadata.camDevices_analog; dataset.metadata.camDevices_digital], ...
                                        ...%[dataset.metadata.scopeDevices; dataset.metadata.camDevices_analog], ...
                                        @(shot, monitor) harvester(shot, monitor, hObject), ...
                                        'DAQ harvester', ...
                                        ...%[1*ones(1,numel(dataset.metadata.scopeDevices)), 2*ones(1,numel(dataset.metadata.camDevices_analog)) ]);
                                        [1*ones(1,numel(dataset.metadata.scopeDevices)), 2*ones(1,numel(dataset.metadata.camDevices_analog)), 3*ones(1,numel(dataset.metadata.camDevices_digital))]);
        shotMonitor.useFastStrategy(true); % use custom strategy for putting together different signals
        shotMonitor.allowManyUpdatesPerCycle(true)
        shotMonitor.setFastStrategyTimeOut(200); % [ms] timeout after which is going to give you data even if incomplete.
        shotMonitor.verbose = true; % gives some info
        
        % save to handles
        handles.shotMonitor = shotMonitor;
        
        % Update handles structure
        guidata(hObject, handles);
        
        % initialize the step functions
        if MD.isScan
            MD.stepFunction(MD.stepVals(handles.progress.step_num1));
            if MD.isScan2D
                MD.stepFunction2(MD.stepVals2(handles.progress.step_num2));
            end
        end
        
        % start monitors
        start(handles.shotMonitor);
        set(handles.acquisitionStatusText, 'String', 'Acquiring', 'ForegroundColor', 'green');
    
    
        
    %% MONITOR CALLBACK FUNCTION
    function harvester(shot, monitor, hObject)
        config = CONFIG();
        
        %disp('harvester')
        %disp(shot)
        %disp(shot.CA_BTV0800.Image.value)
        %disp(shot.CA_BTV0910_DigiCam.ExtractionImage.value)
        %disp(monitor)
        
        % get the updated handle
        handles = guidata(hObject);
        
        % listen for an abort
        if ~~handles.abortButton.UserData % if aborted
            
            % save it to the metadata
            handles.dataset.metadata.aborted = true;
            
            % immediately stop the acquisition
            stopAcquisition(monitor, handles)
            
            % reset the trigger
            set(handles.abortButton,'UserData',false);
            
        else % if not aborted
            
            % reject the very first few shots
            if shot.seqNumber < 2
                dataset.metadata.startTime = datetime(); % reset timer
                
                % update the handles
                guidata(hObject, handles);
                
                return
            end
        
            % save to the dataset
            index = numel(handles.dataset.raw.BSA) + 1;
            handles.dataset.raw.BSA{index} = shot;
            handles.dataset.raw.settings{index} = harvestSettings();
            
            % metadata shortcut
            MD = handles.dataset.metadata;
            
            % save special scalars (shot, step number and value)
            handles.dataset.raw.shot(index) = handles.progress.shots_total;
            if MD.isScan
                handles.dataset.raw.step_num(index) = handles.progress.step_num1;
                handles.dataset.raw.step_value(index) = MD.stepVals(handles.progress.step_num1);
                if MD.isScan2D
                    handles.dataset.raw.step_num2(index) = handles.progress.step_num2;
                    handles.dataset.raw.step_value2(index) = MD.stepVals2(handles.progress.step_num2);
                end
            end
            
            % save arduino gauge data
            for i = 1:size(handles.positionGaugeConnectors,1)
                [x,xu,xt] = getLastValidPos(handles.positionGaugeConnectors{i,2});
                [y,yu,yt] = getLastValidPos(handles.positionGaugeConnectors{i,3});
                %disp(x)
                %disp(y)
                handles.dataset.raw.gaugeX(index)=x;
                handles.dataset.raw.gaugeY(index)=y;
                %TODO: HANDLE MULTIPLE GAUGE SETS
                % Note: This will also challenge assumptions in
                %       DAQ_datasetConverter.m and saveShotToDataset.m
            end
            
            % update the step progress text
            updateStepProgressText(handles, MD);

            % increment shot numbers -- take care of the extra shots needed for synchronization
            extraShots = getLostShotsPerStep(handles);
            if handles.progress.shots_raw_in_step < MD.shotsPerStep + extraShots
                
                % step not complete, keep harvesting
                handles.progress.shots_raw_in_step = handles.progress.shots_raw_in_step + 1;
                handles.progress.shots_raw_total = handles.progress.shots_raw_total + 1;
                
                if handles.progress.shots_in_step < MD.shotsPerStep
                    % if not last shots for synching, increment counter shots as normal
                    handles.progress.shots_in_step = handles.progress.shots_in_step + 1;
                    handles.progress.shots_total = handles.progress.shots_total + 1;
                end
            else
                % step is complete
                if handles.progress.step_num1 < MD.numSteps
                    
                    % still more steps, change to next step
                    handles.progress.step_num1 = handles.progress.step_num1 + 1;
                    handles.progress.shots_raw_in_step = 1;
                    handles.progress.shots_in_step = 1;
                    handles.progress.shots_raw_total = handles.progress.shots_raw_total + 1;
                    handles.progress.shots_total = handles.progress.shots_total + 1;
                    
                    % pause the monitor
                    monitor.pauseOn();
                    set(handles.acquisitionStatusText, 'String', 'Adjusting', 'ForegroundColor', 'yellow');
                    
                    % perform function (parameter 1) after increment
                    MD.stepFunction(MD.stepVals(handles.progress.step_num1));
                    
                    % restart the monitor
                    pause(config.waitTimeTypical);
                    set(handles.acquisitionStatusText, 'String', 'Acquiring', 'ForegroundColor', 'green');
                    monitor.pauseOff();
                    
                else
                    % stepping in 1D is complete
                    if handles.progress.step_num2 < MD.numSteps2
                        
                        % still more steps in the other (2D) direction
                        handles.progress.shots_in_step = 1;
                        handles.progress.shots_raw_in_step = 1;
                        handles.progress.step_num1 = 1;
                        handles.progress.step_num2 = handles.progress.step_num2 + 1;
                        handles.progress.shots_raw_total = handles.progress.shots_raw_total + 1;
                        handles.progress.shots_total = handles.progress.shots_total + 1;
                        
                        % pause the monitor
                        monitor.pauseOn();
                        set(handles.acquisitionStatusText, 'String', 'Adjusting', 'ForegroundColor', 'yellow');

                        % perform function (parameter 2) after increment
                        MD.stepFunction2(MD.stepVals2(handles.progress.step_num2));
                        
                        % perform function (paramater 1) after increment
                        MD.stepFunction(MD.stepVals(handles.progress.step_num1));
                        
                        % restart the monitor
                        %pause(config.waitTimeTypical);
                        set(handles.acquisitionStatusText, 'String', 'Acquiring', 'ForegroundColor', 'green');
                        monitor.pauseOff();
                        
                    else
                        % all done
                        stopAcquisition(monitor, handles)
                        set(handles.acquisitionStatusText, 'String', 'Done!', 'ForegroundColor', 'black');
                    end
                end
            end
        end
        
        % update the handles
        guidata(hObject, handles);
    
        
    
    %% SET METADATA
    function MD = harvestMetadata(handles)
        config = CONFIG();
        
        % declare structures
        MD = struct();
        
        % populate generic metadata
        MD.exptCode = config.expts{handles.exptMenu.Value, 1};
        MD.exptName = config.expts{handles.exptMenu.Value, 2};
        MD.savepath = config.expts{handles.exptMenu.Value, 3};
        MD.printToElog = handles.printToElogCheckbox.Value;
        MD.elogKey = config.expts{handles.exptMenu.Value, 4};
        MD.saveBackground = handles.saveBgCheckbox.Value;
        MD.comment = handles.commentEdit.String;
        MD.aborted = false;
        MD.startTime = datetime();
        
        % populate data to be acquired
        MD.scalarDevices = config.scalars(:,1);
        MD.scalarDesc = config.scalars(:,2);
        MD.scalarVars = config.scalars(:,3);
        MD.scalarCalibrations = config.scalars(:, 4);
        MD.scalarUnits = config.scalars(:, 5);
        
        MD.scopeDevices      = config.scopes(handles.scopeSelectionListbox.Value, 1);
        MD.scopeDesc         = config.scopes(handles.scopeSelectionListbox.Value, 2);
        MD.scopeVars         = config.scopes(handles.scopeSelectionListbox.Value, 3);
        MD.scopeCalibrations = config.scopes(handles.scopeSelectionListbox.Value, 4);
        MD.scopeUnits        = config.scopes(handles.scopeSelectionListbox.Value, 5);
        
        MD.camDevices       = config.cams(handles.camSelectionListbox.Value, 1);
        MD.camDesc          = config.cams(handles.camSelectionListbox.Value, 2);
        MD.camVars          = config.cams(handles.camSelectionListbox.Value, 3);
        MD.camShotOffsets   = config.cams(handles.camSelectionListbox.Value, 4);
        MD.camCalibrationsX = config.cams(handles.camSelectionListbox.Value, 5);
        MD.camCalibrationsY = config.cams(handles.camSelectionListbox.Value, 6);
        MD.camTransposed    = config.cams(handles.camSelectionListbox.Value, 7);
        MD.camFlipLR        = config.cams(handles.camSelectionListbox.Value, 8);
        MD.camFlipUD        = config.cams(handles.camSelectionListbox.Value, 9);
        MD.camIsDigital     = config.cams(handles.camSelectionListbox.Value, 10);

        MD.camDevices_analog  = MD.camDevices(~cell2mat(MD.camIsDigital));
        MD.camDevices_digital = MD.camDevices(cell2mat(MD.camIsDigital));
        
        % determine steps and shots
        MD.shotsPerStep = round(str2double(handles.shotsPerStepEdit.String));
        MD.lostShotsPerStep = getLostShotsPerStep(handles);
        MD.isScan1D = ~~handles.scanTypeScan1D.Value;
        MD.isScan2D = ~~handles.scanTypeScan2D.Value;
        MD.isScan = MD.isScan1D || MD.isScan2D;
        
        % set steps and shots based on scan type
        if MD.isScan
            MD.scanName = config.scans{handles.param1menu.Value, 1};
            MD.stepVals = handles.steps1text.UserData;
            MD.isLogScan = handles.log1RadioButton.Value;
            MD.stepFunction = config.scans{handles.param1menu.Value, 2};
            MD.stepUnit = config.scans{handles.param1menu.Value, 3};
            MD.numSteps = numel(MD.stepVals);
            if MD.isScan2D
                MD.scanType = '2D scan';
                MD.scanName2 = config.scans{handles.param2menu.Value, 1};
                MD.stepVals2 = handles.steps2text.UserData;
                MD.isLogScan2 = handles.log2RadioButton.Value;
                MD.stepFunction2 = config.scans{handles.param2menu.Value, 2};
                MD.stepUnit2 = config.scans{handles.param2menu.Value, 3};
                MD.numSteps2 = numel(MD.stepVals2);
                MD.scanType = '2D scan';
            elseif MD.isScan1D
                MD.numSteps2 = 1;
                MD.scanType = 'Scan';
            end
        else
            MD.numSteps = 1;
            MD.numSteps2 = 1;
            MD.scanType = 'Simple DAQ';
        end
        MD.numShotsRequested = MD.numSteps2*MD.numSteps*MD.shotsPerStep; % total number of shots
    
    
    %% SET UP THE DATASET STRUCTURE
    function [scalars, scopes, images] = setupDatasetStructure(MD)
        
        % declare structures
        [scalars, scopes, images] = deal(struct());
        
        % populate scans with step scalars
        scalars.shot.desc = 'Shot number';
        if MD.isScan
            scalars.step_num.desc = 'Step number';
            scalars.step_value.desc = 'Step value';
            scalars.step_value.unit = MD.stepUnit;
            if MD.isScan2D
                scalars.step_num2.desc   = 'Step number (parameter 2)';
                scalars.step_value2.desc = 'Step value (parameter 2)';
                scalars.step_value2.unit = MD.stepUnit2;
            end
        end
        
        % set up the scalars
        for l = 1:numel(MD.scalarDevices)
            scalars.(MD.scalarVars{l}).desc = MD.scalarDesc{l};
            scalars.(MD.scalarVars{l}).device = MD.scalarDevices{l};
            scalars.(MD.scalarVars{l}).unit = MD.scalarUnits{l};
        end
        
        % set up the scopes
        for l = 1:numel(MD.scopeDevices)
            scopes.(MD.scopeVars{l}).desc = MD.scopeDesc{l};
            scopes.(MD.scopeVars{l}).device = MD.scopeDevices{l};
            scopes.(MD.scopeVars{l}).unit = MD.scopeUnits{l};
            scopes.(MD.scopeVars{l}).unitsPerVolt = MD.scopeCalibrations{l};
        end
        
        % set up the cameras
        for l = 1:numel(MD.camDevices)
            images.(MD.camVars{l}).desc = MD.camDesc{l};
            images.(MD.camVars{l}).device = MD.camDevices{l};
        end
        
        
        
    %% SET UP STEP PROGRESS TEXT
    function setupStepProgressText(handles, MD)
        config = CONFIG();
        
        % turn on progress panel
        set(handles.progressPanel, 'Visible', 'on');
        
        % update type of scan
        set(handles.scanTypeValue, 'String', MD.scanType);
        
        % switch on correct panels given the scan type
        if MD.isScan
            set(handles.stepProgressText,   'Visible', 'on');
            set(handles.stepProgressValue1, 'Visible', 'on');
            if MD.isScan2D
                set(handles.stepProgressValue2, 'Visible', 'on');
            elseif MD.isScan1D
                set(handles.stepProgressValue2, 'Visible', 'off');
            end
        else
            set(handles.stepProgressText,   'Visible', 'off');
            set(handles.stepProgressValue1, 'Visible', 'off');
            set(handles.stepProgressValue2, 'Visible', 'off');
        end   
        
        % update cam and scope selected panels
        set(handles.camSelectedText, 'String', config.cams(handles.camSelectionListbox.Value, 2));
        set(handles.scopeSelectedText, 'String', config.scopes(handles.scopeSelectionListbox.Value, 2));
        
    
        
    %% UPDATE THE STEP PROGRESS TEXTS
    function updateStepProgressText(handles, MD)
        
        % write the shot progress panel
        shots_requested = MD.numShotsRequested;
        shots_collected = handles.progress.shots_total;
        if MD.isScan
            shots_in_step_requested = MD.shotsPerStep;
            shots_in_step_collected = handles.progress.shots_in_step;
            shotstr = [num2str(shots_in_step_collected) '/' num2str(shots_in_step_requested) ...
                            ' (' num2str(shots_collected) '/' num2str(shots_requested) ')'];
        else
            shotstr = [num2str(shots_collected) '/' num2str(shots_requested)];
        end
        set(handles.shotProgressValue, 'String', shotstr);
        
        % update the step value progress
        if MD.isScan
            % steps
            step_num1 = handles.progress.step_num1;
            step_val1 = MD.stepVals(step_num1);
            set(handles.stepProgressValue1, 'String', ...
                [num2str(step_num1) '/' num2str(MD.numSteps) ' (' num2str(step_val1, '%g') ' ' MD.stepUnit ')']);
            if MD.isScan2D
                step_num2 = handles.progress.step_num2;
                step_val2 = MD.stepVals2(step_num2);
                set(handles.stepProgressValue2, 'String', ...
                    [num2str(step_num2) '/' num2str(MD.numSteps2) ' (' num2str(step_val2, '%g') ' ' MD.stepUnit2 ')']);
            end
        end
        
        % update time remaining (and generate time word)
        timePerShot = seconds(datetime() - MD.startTime)/shots_collected; % [s]
        timeRemaining = (shots_requested - shots_collected)*timePerShot; % [s]
        if timeRemaining < 60
            timeText = [num2str(ceil(timeRemaining)) ' sec'];
        elseif timeRemaining < 3600
            timeText = [num2str(ceil(timeRemaining/60)) ' min'];
        elseif timeRemaining < 36000
            timeText = [num2str(ceil(timeRemaining/3600)) ' hours'];
        else
            timeText = 'Better get the popcorn!';
        end
        set(handles.timeLeftText, 'String', timeText);

    
    
    %% STOP ACQUISITION and RESET
    function stopAcquisition(monitor, handles)
           
        % stop and delete the monitor
        stop(monitor);

        % stop arduino gauge connectors
        for i = 1:size(handles.positionGaugeConnectors,1)
            delete(handles.positionGaugeConnectors{i,2})
            delete(handles.positionGaugeConnectors{i,3})
        end
            
        % reset buttons
        set(handles.acquisitionStatusText, 'String', 'Ready', 'ForegroundColor', 'black');
        set(handles.timeLeftText, 'String', '');
        set(handles.abortButton,  'Enable','off');
        set(handles.acquireButton,'Enable','on');
        
        % finalize dataset information
        dataset = handles.dataset;
        dataset.metadata.numShots_raw = handles.progress.shots_raw_total - dataset.metadata.aborted;
        dataset.metadata.endTime = datetime();
        dataset.metadata.duration = seconds(dataset.metadata.endTime - dataset.metadata.startTime); % [s]
        dataset.metadata.dataset_id = datasetCounter(dataset.metadata);
        
        % save dataset to file
        saveDataset(dataset, handles);
        
    %% SAVE THE DATASET TO FILE
    function saveDataset(dataset, handles)
        
        % save dataset to file
        savepath = dataset.metadata.savepath;
        filedir_raw = [savepath 'raw/' datestr(now, 'ddmmyyyy')];
        % make folder if not there
        if ~exist(filedir_raw,'dir')
            mkdir(filedir_raw)
        end
        filename_raw = [filedir_raw '/' num2str(dataset.metadata.dataset_id) '_raw.mat'];
        dataset.metadata.savepath_raw = filename_raw;
        save(filename_raw, 'dataset', '-v7.3');
        fprintf('Wrote raw dataset to file\n\t''%s''\n', filename_raw);
        
        % run conversion script to calibrate & resynchronize the data
        % to make a nice package for off-line processing
        [~, dataset] = DAQ_datasetConverter(filename_raw);
        
        % print to elog
        if dataset.metadata.printToElog
            printToElogbook(dataset);
        end
        
        % update status to show dataset ID
        set(handles.timeLeftText, 'String', ['Saved dataset ' num2str(dataset.metadata.dataset_id)]);
        
        
    %% HARVEST THE SETTINGS
    function settings = harvestSettings()
        config = CONFIG();
        
        % get scalars
        signals = matlabJapcGetSignals('SCT.USER.SETUP', config.scalars(:,1));
        settings = signals{1}; % only one shot
        
        
        
    %% HARVEST THE MACHINE STATE
    function state = harvestMachineState()
        config = CONFIG();
        
        % get variables from the config
        stateDevices = config.machineState(:,1);
        stateDesc = config.machineState(:,2);
        stateVars = config.machineState(:,3);
        
        % get current state of the machine variables
        %signals = matlabJapcGetSignals('SCT.USER.ALL', stateDevices);
        signals = matlabJapcGetSignals('SCT.USER.SETUP', stateDevices);
        machine = signals{1};
        % arrange neatly
        for i = 1:numel(stateDevices)
            state.(stateVars{i}).desc = stateDesc{i};
            state.(stateVars{i}).device = stateDevices{i};
            % extract signal
            strs1 = strsplit(stateDevices{i},'/');
            strs2 = strsplit(strs1{2},'#');
            device = strrep(strrep(strs1{1},'.','_'),'-','_');
            state.(stateVars{i}).dat = machine.(device).(strs2{1}).(strs2{2}).value;
        end
        
        
    %% ABORT BUTTON
    function abortButton_Callback(hObject, eventdata, handles)  %#ok<DEFNU>
        set(hObject,'UserData', true);      
      
     
    %% CHANGE IN PARAMETER MENU 1
    function param1menu_Callback(hObject, eventdata, handles)
        config = CONFIG();
        unit = config.scans{hObject.Value,3};
        initDefault = config.scans{hObject.Value,4};
        finalDefault = config.scans{hObject.Value,5};
        if ~isnan(config.scans{hObject.Value,6})
        	numStepsDefault = config.scans{hObject.Value,6};
        else
            numStepsDefault = config.defaults.numSteps;
        end

        % update param #1 input fields (initial and final values, num steps)
        set(handles.initStep1edit, 'String', initDefault);
        set(handles.initStep1UnitText, 'String', unit);
        set(handles.finalStep1edit, 'String', finalDefault);
        set(handles.finalStep1UnitText, 'String', unit);
        set(handles.numStep1edit, 'String', numStepsDefault);
        
        updateParam1steps(handles);
    
        
        
    %% CHANGE IN PARAMETER MENU 1 
    function param2menu_Callback(hObject, eventdata, handles)
        config = CONFIG();
        unit = config.scans{hObject.Value,3};
        initDefault = config.scans{hObject.Value,4};
        finalDefault = config.scans{hObject.Value,5};

        % update param #1 input fields (initial and final values, num steps)
        set(handles.initStep2edit, 'String', initDefault);
        set(handles.initStep2UnitText, 'String', unit);
        set(handles.finalStep2edit, 'String', finalDefault);
        set(handles.finalStep2UnitText, 'String', unit);
        set(handles.numStep2edit, 'String', config.defaults.numSteps);
        
        updateParam2steps(handles);

        

    %% SETS UP THE SHOTS PER STEP INPUT
    function shotsPerStepEdit_CreateFcn(hObject, eventdata, handles)  %#ok<DEFNU>
        config = CONFIG();
        set(hObject, 'String', config.defaults.shotsPerStep);
        
        
    %% SETS UP THE ABORT BUTTON
    function abortButton_CreateFcn(hObject, eventdata, handles) %#ok<DEFNU>
        set(hObject,'Enable','off');

    % --- Executes on button press in scanTypeSimpleDAQ.
    function scanTypeSimpleDAQ_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        set(handles.scanParam1panel, 'Visible', 'off');
        set(handles.scanParam2panel, 'Visible', 'off');
        updateTimeEstimate(handles);

    % --- Executes on button press in scanTypeScan1D.
    function scanTypeScan1D_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        set(handles.scanParam1panel, 'Visible', 'on');
        set(handles.scanParam2panel, 'Visible', 'off');
        updateParam1steps(handles);

    % --- Executes on button press in scanTypeScan2D.
    function scanTypeScan2D_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        set(handles.scanParam1panel, 'Visible', 'on');
        set(handles.scanParam2panel, 'Visible', 'on');
        updateParam1steps(handles);
        updateParam2steps(handles);

        
    % update the steps for any change of parameter #1
    function initStep1edit_Callback(~, ~, handles);        updateParam1steps(handles); %#ok<DEFNU>
    function finalStep1edit_Callback(~, ~, handles);       updateParam1steps(handles); %#ok<DEFNU>
    function numStep1edit_Callback(~, ~, handles);         updateParam1steps(handles); %#ok<DEFNU>
    function numPoints1menu_Callback(~, ~, handles);       updateParam1steps(handles); %#ok<DEFNU>
    function manual1edit_Callback(~, ~, handles);          updateParam1steps(handles); %#ok<DEFNU>
    function lin1RadioButton_Callback(~, ~, handles);      updateParam1steps(handles); %#ok<DEFNU>
    function log1RadioButton_Callback(~, ~, handles);      updateParam1steps(handles); %#ok<DEFNU>
    function manual1RadioButton_Callback(~, ~, handles);   updateParam1steps(handles); %#ok<DEFNU>
    function forward1radiobutton_Callback(~, ~, handles);  updateParam1steps(handles); %#ok<DEFNU>
    function backward1radiobutton_Callback(~, ~, handles); updateParam1steps(handles); %#ok<DEFNU>
    function both1radiobutton_Callback(~, ~, handles);     updateParam1steps(handles); %#ok<DEFNU>
    function random1radiobutton_Callback(~, ~, handles);   updateParam1steps(handles); %#ok<DEFNU>
        
    % update the steps for any change of parameter #2
    function initStep2edit_Callback(~, ~, handles);        updateParam2steps(handles); %#ok<DEFNU>
    function finalStep2edit_Callback(~, ~, handles);       updateParam2steps(handles); %#ok<DEFNU>
    function numStep2edit_Callback(~, ~, handles);         updateParam2steps(handles); %#ok<DEFNU>
    function numPoints2menu_Callback(~, ~, handles);       updateParam2steps(handles); %#ok<DEFNU>
    function lin2RadioButton_Callback(~, ~, handles);      updateParam2steps(handles); %#ok<DEFNU>
    function log2RadioButton_Callback(~, ~, handles);      updateParam2steps(handles); %#ok<DEFNU>
    function forward2radiobutton_Callback(~, ~, handles);  updateParam2steps(handles); %#ok<DEFNU>
    function backward2radiobutton_Callback(~, ~, handles); updateParam2steps(handles); %#ok<DEFNU>
    function both2radiobutton_Callback(~, ~, handles);     updateParam2steps(handles); %#ok<DEFNU>
    function random2radiobutton_Callback(~, ~, handles);   updateParam2steps(handles); %#ok<DEFNU>

	% update the time estimate for any general parameter
    function shotsPerStepEdit_Callback(~, ~, handles);     updateTimeEstimate(handles);%#ok<DEFNU>

        
    % helper function when converting list to string
    function out = removeLastComma(s); out = s(1:(end-1));
    
    % updates the steps in parameter #1
    function updateParam1steps(handles)
        % get initial and final values and how many
        initStep = str2double(handles.initStep1edit.String);
        finalStep = str2double(handles.finalStep1edit.String);


        % choose linear or logarithmic scale
        if ~~handles.lin1RadioButton.Value % linear
            set(handles.numPoints1menu, 'Enable', 'on');
            if handles.numPoints1menu.Value == 1
                numSteps = ceil(abs(str2double(handles.numStep1edit.String)));
                steps = linspace(initStep, finalStep, numSteps);
            else
                stepSize = str2double(handles.numStep1edit.String);
                steps = initStep:stepSize:finalStep;
            end
            
            set(handles.manual1edit,'Enable','off');
        elseif ~~handles.log1RadioButton.Value % logarithmic
            set(handles.numPoints1menu, 'Enable', 'off');
            handles.numPoints1menu.Value = 1.0;
            
            numSteps = ceil(abs(str2double(handles.numStep1edit.String)));
            steps = logspace(log10(initStep), log10(finalStep), numSteps);
            set(handles.manual1edit,'Enable','off');
        elseif ~~handles.manual1RadioButton.Value % manual steps (from manual1edit)
            set(handles.numPoints1menu, 'Enable', 'off');
            handles.numPoints1menu.Value = 1.0;
            
            set(handles.manual1edit,'Enable','on');            
            %Convert the comma-separated list of number to a string
            steps = double(str2num(handles.manual1edit.String));
        end

        % choose scan direction
        if ~~handles.forward1radiobutton.Value % forward
            % nothing happens
        elseif ~~handles.backward1radiobutton.Value % backward
            steps = fliplr(steps);
        elseif ~~handles.both1radiobutton.Value % both
            steps = [steps fliplr(steps(1:(end-1)))];
        elseif ~~handles.random1radiobutton.Value % random
             steps = steps(randperm(numel(steps)));
        end
        
        % update steps1panel title
        set(handles.steps1panel, 'Title', ['Steps, N=', num2str(length(steps))]);
        
        % update step field and its UserData for later access
        set(handles.steps1text, 'String', removeLastComma(num2str(steps, '%g, ')));
        set(handles.steps1text, 'UserData', steps);

        % update time estimate field
        updateTimeEstimate(handles);
        
        
    % updates the steps in parameter #2
    function updateParam2steps(handles)
        % get initial and final values and how many
        initStep = str2double(handles.initStep2edit.String);
        finalStep = str2double(handles.finalStep2edit.String);

        % choose linear or logarithmic scale
        if ~~handles.lin2RadioButton.Value % linear
            set(handles.numPoints2menu, 'Enable', 'on');
            if handles.numPoints2menu.Value == 1
                numSteps = ceil(abs(str2double(handles.numStep2edit.String)));
                steps = linspace(initStep, finalStep, numSteps);
            else
                stepSize = str2double(handles.numStep2edit.String);
                steps = initStep:stepSize:finalStep;
            end            
        elseif ~~handles.log2RadioButton.Value % logarithmic
            numSteps = ceil(abs(str2double(handles.numStep2edit.String)));
            steps = logspace(log10(initStep), log10(finalStep), numSteps);

            set(handles.numPoints2menu, 'Enable', 'off');
            handles.numPoints2menu.Value = 1.0;
        end

        % choose scan direction
        if ~~handles.forward2radiobutton.Value % forward
            % nothing happens
        elseif ~~handles.backward2radiobutton.Value % backward
            steps = fliplr(steps);
        elseif ~~handles.both2radiobutton.Value % both
            steps = [steps fliplr(steps(1:(end-1)))];
        elseif ~~handles.random2radiobutton.Value % random
             steps = steps(randperm(numel(steps)));
        end

        % update steps2panel title
        set(handles.steps2panel, 'Title', ['Steps, N=', num2str(length(steps))]);
        
        % update step field and its UserData for later access
        set(handles.steps2text, 'String', removeLastComma(num2str(steps, '%g, ')));
        set(handles.steps2text, 'UserData', steps);

        % update time estimate field
        updateTimeEstimate(handles);

    function lostShotsPerStep = getLostShotsPerStep(handles)
        config = CONFIG();
        
        camShotOffsets = config.cams(handles.camSelectionListbox.Value, 4);
        if isempty(camShotOffsets)
            lostShotsPerStep = 0;
        else
            lostShotsPerStep = max([camShotOffsets{:}])-min(min([camShotOffsets{:}]), 0);
        end
        
    % updates the time estimate, triggered by various sources
    function updateTimeEstimate(handles)
        % get config
        config = CONFIG();
        
        % calculate total number of shots
        shotsPerStep = str2double(handles.shotsPerStepEdit.String) +1;
        if ~~handles.scanTypeScan1D.Value || ~~handles.scanTypeScan2D.Value
            numSteps1 = numel(handles.steps1text.UserData); 
        else
            numSteps1 = 1;
        end
        if ~~handles.scanTypeScan2D.Value
            numSteps2 = numel(handles.steps2text.UserData); 
        else
            numSteps2 = 1;
        end
        totalShots = ceil(shotsPerStep * numSteps1 * numSteps2);
        
        % take into account the number of extra shots needed for resynchronization
        lostShotsPerStep = getLostShotsPerStep(handles);
        lostShots = lostShotsPerStep*numSteps1*numSteps2;
        
        % get machine rep rate and estimate time (in seconds)
        repRate = 1/config.waitTimeTypical; % [Hz]
        timeEstimate = (totalShots+lostShots)/repRate; % [s]
        if timeEstimate < 60
            timeText = [num2str(ceil(timeEstimate)) ' sec'];
        elseif timeEstimate < 3600
            timeText = [num2str(ceil(timeEstimate/60)) ' min'];
        elseif timeEstimate < 36000
            timeText = [num2str(ceil(timeEstimate/3600)) ' hours'];
        else
            timeText = 'Sometime after Christmas';
        end

        % update total shots field and completion time estimate
        set(handles.totalShotsText, 'String', [num2str(totalShots) ' (+' num2str(lostShots) ')']);
        set(handles.completionTimeText, 'String', timeText);

        
    % button to immediately set the scan value (param 2) to the initial value
    function scan2setNowButton_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        config = CONFIG();
        scan2function = config.scans{handles.param2menu.Value, 2};
        scan2initValue = str2double(handles.initStep2edit.String);
        scan2function(scan2initValue);

    % button to immediately set the scan value (param 1) to the initial value
    function scan1setNowButton_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        config = CONFIG();
        scan1function = config.scans{handles.param1menu.Value, 2};
        scan1initValue = str2double(handles.initStep1edit.String);
        scan1function(scan1initValue);

    % button to immediately set the scan value (param 2) to the final value
    function scan2setNowButton2_Callback(hObject, eventdata, handles) %#ok<DEFNU>
        config = CONFIG();
        scan2function = config.scans{handles.param2menu.Value, 2};
        scan2finalValue = str2double(handles.finalStep2edit.String);
        scan2function(scan2finalValue);

    % button to immediately set the scan value (param 1) to the final value
    function scan1setNowButton2_Callback(hObject, eventdata, handles)
        config = CONFIG();
        scan1function = config.scans{handles.param1menu.Value, 2};
        scan1finalValue = str2double(handles.finalStep1edit.String);
        scan1function(scan1finalValue);
        
    % Update the number of extra shots per step needed for
    % resynchronization when the camera selection is updated.
    function camSelectionListbox_Callback(hObject, eventdata, handles); updateTimeEstimate(handles)

    % --- Executes on selection change in exptMenu.
    function exptMenu_Callback(hObject, eventdata, handles)
        % hObject    handle to exptMenu (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    structure with handles and user data (see GUIDATA)

        % Hints: contents = cellstr(get(hObject,'String')) returns exptMenu contents as cell array
        %        contents{get(hObject,'Value')} returns selected item from exptMenu

        expID = get(hObject,'Value');
        changeExperiment(expID, handles)
    
    function changeExperiment(expID, handles)
        %Update the default cameras and scopes
        config = CONFIG();
        defCams   = config.expts{expID,5};
        defScopes = config.expts{expID,6};
        
        defCamsID = [];
        for cNAM = defCams
            for i = 1:length(config.cams)
                if strcmp(cNAM,config.cams{i,3})
                    defCamsID(end+1) = i; %#ok<AGROW>
                end
            end
        end
        
        defScopesID = [];
        for sNAM = defScopes
            for i = 1:length(config.scopes)
                if strcmp(sNAM,config.scopes{i,3})
                    defScopesID(end+1) = i; %#ok<AGROW>
                end
            end
        end
        
        handles.camSelectionListbox.Value   = defCamsID;
        handles.scopeSelectionListbox.Value = defScopesID;
