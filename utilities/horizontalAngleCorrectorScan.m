function [] = horizontalAngleCorrectorScan(angle_mrad)
    
    % calibrations
    offset_710 = 0.8; % [A]
    offset_780 = 0; % [A]
    ratio = -0.264; % change in 710 for every A in 780
    cal_angular = 1/0.776; % [A in 780 / mrad] 
    
    % calculate the currents
    current_H_710 = offset_710 + cal_angular*angle_mrad*ratio;
    current_H_780 = offset_780 + cal_angular*angle_mrad;
    
    % set the currents
    setVariable( 'CA.DHJ0710', 'SettingPPM', 'current', current_H_710, 1);
    setVariable( 'CA.DHJ0780', 'SettingPPM', 'current', current_H_780, 1);
    
end

