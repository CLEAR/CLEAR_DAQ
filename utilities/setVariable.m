function [] = setVariable( device, property, item, value, waitForUpdate )

    if ~exist('waitForUpdate','var'); waitForUpdate = false; end;
    
    % construct signal name
    signalName = [device '/' property '#' item];
    
    % if waiting for update, get initial value
    %if waitForUpdate; initValue = getVariable(device, property, item); end
    
    % set the value
    %matlabJapc.staticSetSignal('SCT.USER.SETUP', signalName, value);
    dst = device(1:4);
    if strcmp(dst, 'CA.Q') || strcmp(dst, 'CA.D') || strcmp(dst, 'CA.B') || strcmp(dst, 'CL.E') || strcmp(dst, 'CK.L') ||strcmp(device(1:6), 'CA.MOV')
        % set magnets and movers
        matlabJapc.staticSetSignal('', signalName, value);
    else
        % set all other things
        matlabJapc.staticSetSignal('SCT.USER.ALL', signalName, value);
    end
    
    % optional wait for reaching signal
    tic; % start timer
    threshold = 1e-5;
    checkInterval = 0.5; % [s]
    maxTime = 15; % [s]
    if waitForUpdate
        while toc < maxTime
            
            if strcmp(property, 'CMD') && strcmp(item, 'requestedPosition')
                finalValue = getVariable(device, 'AQN', 'actualPosition');
            elseif strcmp(property, 'SettingPPM') && strcmp(item, 'current')
                threshold = 2e-2;
                maxTime = 2; % [s]
                finalValue = getVariable(device, 'Acquisition', 'currentAverage');
            else
                finalValue = getVariable(device, property, item);
            end
            
            if abs(finalValue - value) < threshold
                return;
            else
                pause(checkInterval);
                if strcmp(dst, 'CA.Q') || strcmp(dst, 'CA.D') || strcmp(dst, 'CA.B') || strcmp(dst, 'CL.E') || strcmp(dst, 'CK.L') ||strcmp(device(1:6), 'CA.MOV')
                    % set magnets
                    matlabJapc.staticSetSignal('', signalName, value);
                else
                    % set all other things
                    matlabJapc.staticSetSignal('SCT.USER.ALL', signalName, value);
                end
            end
        end
    end

end

