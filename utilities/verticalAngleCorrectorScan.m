function [] = verticalAngleCorrectorScan(angle_mrad)
    
    % calibrations
    %offset_710 = 0.3; % [A]
    %offset_780 = 0; % [A]
    ratio = -1.9; % change in 710 for every A in 780
    cal_angular = 1; % [A in 780 / mrad] 
    
    % calculate the currents
    current_V_710 = cal_angular*angle_mrad;
    current_V_780 = cal_angular*angle_mrad*ratio;
    
    % set the currents
    setVariable( 'CA.DVJ0710', 'SettingPPM', 'current', current_V_710, 1);
    setVariable( 'CA.DVJ0780', 'SettingPPM', 'current', current_V_780, 1);
    
end

