function [] = moverHorMetricScanFunction( offset_um )

    % get discharge setting and turn off before moving
    wasDischarging = matlabJapc.staticGetSignal('', 'CL.ECLCNT1/Enabled#enabled');
    matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', false);

    % get reference position of mover
    device = 'CA.MOV0800-H';
    refLSA = matlabLSA.staticGetRefSignal('SCT.USER.ALL', [device '/CMD#requestedPosition']);
    refPosition = refLSA.value; % [steps]
    
    % determine offset calibration (varies with step length)
    targetPosition = matlabJapc.staticGetSignal('', [device '/AQN#targetPosition']);
    cal_y0 = 1.00;
    prev_offset_um = (refPosition-targetPosition)/cal_y0; % approximate
    stepsize = abs(offset_um-prev_offset_um);
    if stepsize < 35
        cal_x = 0.88; % [steps/um]
    elseif stepsize < 100
        cal_x = 0.94; % [steps/um]
    elseif stepsize < 350
        cal_x = 0.96; % [steps/um] 
    else
        cal_x = 1.00; % [steps/um] 
    end
    
    % perform step
    setVariable( device , 'CMD', 'requestedPosition', refPosition+offset_um*cal_x, 1);
    
    % re-establish
    matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', wasDischarging);
    
end

