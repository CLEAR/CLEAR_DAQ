function [nextDataset] = datasetCounter(MD)
    
    % load or make new dataset file
    config = CONFIG();
    try
        load(config.datasetcounter_file, 'lastDataset', 'datasetList');
    catch
        lastDataset = 10000;
        datasetList = cell(0);
    end
    
    % increment dataset number
    nextDataset = lastDataset + 1;
    lastDataset = nextDataset;
    
    % prepare scan text
    if ~~MD.isScan2D
        scanText = ['2D scan - ' MD.scanName ' & ' MD.scanName2];
    elseif ~~MD.isScan1D
        scanText = ['Scan - ' MD.scanName];
    else
        scanText = 'Simple DAQ';
    end
    
    % append a record of the most important info
    datasetList(size(datasetList,1)+1,:) = {nextDataset, MD.exptName, MD.startTime, MD.numShots_raw, scanText, ~MD.aborted};

    % save back into the dataset file
    save(config.datasetcounter_file, 'lastDataset', 'datasetList');
    
end

