function [ value ] = getVariable( device, property, item )

    % defaults
    if ~exist('property','var'); property = ''; end;
    if ~exist('item','var'); item = ''; end;
    
    % construct signal name
    signalName = device;
    if ~strcmp(property,'') 
        signalName = [signalName '/' property];
        if ~strcmp(item,'')
            signalName = [signalName '#' item]; 
        end
    end 
    
    % get the value
    %value = matlabJapc.staticGetSignal('SCT.USER.SETUP', signalName);
    dst = device(1:4);
    if strcmp(dst, 'CA.Q') || strcmp(dst, 'CA.D') || strcmp(dst, 'CA.B') || strcmp(dst, 'CL.E') || strcmp(dst, 'CK.L') || strcmp(device(1:6), 'CA.MOV')
        % read magnets and movers
        value = matlabJapc.staticGetSignal('SCT.USER.SETUP', signalName);
    else
        % read all other things
        value = matlabJapc.staticGetSignal('SCT.USER.ALL', signalName);
    end
    
end

