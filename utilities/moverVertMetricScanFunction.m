function [] = moverVertMetricScanFunction( offset_um )
    
    % get discharge setting and turn off before moving
    wasDischarging = matlabJapc.staticGetSignal('', 'CL.ECLCNT1/Enabled#enabled');
    matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', false);
    
    % get reference position of mover
    device = 'CA.MOV0800-V';
    refLSA = matlabLSA.staticGetRefSignal('SCT.USER.ALL', [device '/CMD#requestedPosition']);
    refPosition = refLSA.value; % [steps]
    
    % determine offset calibration (varies with step length)
    targetPosition = matlabJapc.staticGetSignal('', [device '/AQN#targetPosition']);
    cal_y0 = -2.55;
    prev_offset_um = (refPosition-targetPosition)/cal_y0; % approximate
    stepsize = abs(offset_um-prev_offset_um);
    if stepsize < 35
        cal_y = -2.48; % [steps/um]
    elseif stepsize < 200
        cal_y = -2.52; % [steps/um]
    elseif stepsize < 600
        cal_y = -2.56; % [steps/um] 
    else
        cal_y = -2.60; % [steps/um] 
    end
    
    % perform step
    setVariable( device , 'CMD', 'requestedPosition', refPosition+offset_um*cal_y, 1)
    
    % re-establish
    matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', wasDischarging);
    
end

