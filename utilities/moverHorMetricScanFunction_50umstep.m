function [] = moverHorMetricScanFunction_50umstep( offset_um, refValue )

    % get discharge setting and turn off before moving
    %wasDischarging = matlabJapc.staticGetSignal('', 'CL.ECLCNT1/Enabled#enabled');
    %  matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', false);

    % get reference position of mover
    device = 'CA.MOV0800-H';
    if exist('refValue', 'var')
        refPosition = refValue;
    else
        refLSA = matlabLSA.staticGetRefSignal('SCT.USER.ALL', [device '/CMD#requestedPosition']);
        refPosition = refLSA.value; % [steps]
    end
    
    % determine offset calibration (varies with step length)
    currentStep = matlabJapc.staticGetSignal('', [device '/AQN#targetPosition']);
    cal_x = 0.930; % [steps/um] for 50 um steps 
    stepsize_um = 50; % [um] 
    
    % do multiple small steps
    finalStep = refPosition+offset_um*cal_x;
    dx_steps = sign(cal_x*(finalStep-currentStep))*stepsize_um*cal_x;
    steps = currentStep:dx_steps:finalStep;
    
    if length(steps) == 1
        steps = [currentStep,finalStep];
    elseif length(steps) > 0
        steps(end) = finalStep;
    end
    
    for i = 2:numel(steps)
        % perform step
        setVariable( device , 'CMD', 'requestedPosition', steps(i), 1);
        pause(3);
    end
    
    % re-establish
    %matlabJapc.staticSetSignal('', 'CL.ECLCNT1/Enabled#enabled', wasDischarging);
    %pause(1.5);
end

