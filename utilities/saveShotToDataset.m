function [ dataset ] = saveShotToDataset(dataset, index_raw, camShotOffsets, config)
    %saveShotToDataset - Reads the raw data and spits out calibrated
    % and time-aligned data for the analysis the chew on.
    
    % config: if not NaN, a new config to load the camera- and scope
    % callibrations from.
    
    % increment the total number of shots in dataset
    dataset.metadata.numShots = dataset.metadata.numShots + 1;
    index_proc = dataset.metadata.numShots;
    
    
    %% METADATA
    % load the metadata
    scalarDevices = dataset.metadata.scalarDevices;
    scalarVars = dataset.metadata.scalarVars;
    scalarCalibrations = dataset.metadata.scalarCalibrations;
    scopeDevices = dataset.metadata.scopeDevices;
    scopeVars = dataset.metadata.scopeVars;
    if isstruct(config)
        %find the right scopes to use
        idx = [];
        for sc1 = 1:size(config.scopes,1)
            for sc2 = 1:length(dataset.metadata.scopeVars)
                if strcmp(config.scopes{sc1,3}, dataset.metadata.scopeVars(sc2))
                    idx = [idx, sc1]; %#ok<AGROW> 
                end
            end
        end
        
        %Load new calibration
        dataset.metadata.scopeCalibrations = config.scopes(idx, 4);
    end
    scopeCalibrations = dataset.metadata.scopeCalibrations;
    
    camDevices = dataset.metadata.camDevices;
    camVars = dataset.metadata.camVars;
    
    if isstruct(config)
        %find the right cameras to use
        idx = [];
        for cc1 = 1:size(config.cams,1)
            for cc2 = 1:length(dataset.metadata.camVars)
                if strcmp(config.cams{cc1,3}, dataset.metadata.camVars(cc2))
                    idx = [idx, cc1]; %#ok<AGROW> 
                end
            end
        end
        
        %Check that we found everything
        if length(dataset.metadata.camVars) ~= length(idx)
            error('Dit not find all cameras from the file in the new CONFIG.')
        end
        
        %Load new calibrations
        dataset.metadata.camCalibrationsX = config.cams(idx, 5);
        dataset.metadata.camCalibrationsY = config.cams(idx, 6);
        dataset.metadata.camTransposed    = config.cams(idx, 7);
        dataset.metadata.camFlipLR        = config.cams(idx, 8);
        dataset.metadata.camFlipUD        = config.cams(idx, 9);
    end
    
    
    %% SCALARS
    % set up the special scalars: shot, step number and step value
    dataset.scalars.shot.dat(index_proc) = dataset.raw.shot(index_raw);
    if dataset.metadata.isScan
        % parameter 1
        dataset.scalars.step_num.dat(index_proc) = dataset.raw.step_num(index_raw);
        dataset.scalars.step_value.dat(index_proc) = dataset.raw.step_value(index_raw);
        if dataset.metadata.isScan2D
            % parameter 2
            dataset.scalars.step_num2.dat(index_proc) = dataset.raw.step_num2(index_raw);
            dataset.scalars.step_value2.dat(index_proc) = dataset.raw.step_value2(index_raw);
        end
    end
    
    % add gauges, if present
    if isfield(dataset.raw,'gaugeX')
        if ~isfield(dataset.raw,'gaugeY')
            error('Found gaugeX but not gaugeY, weird!')
        end

        %TODO: Add direction-calibration
        gaugeX_callib = 1.0;
        gaugeY_callib = 1.0;
        if isstruct(config) %TODO: This should always happen...
            % TODO: Currently assuming a single gauge
            posGaugeIdx = 1;
            gaugeX_callib = config.posgauges{posGaugeIdx,5};
            gaugeY_callib = config.posgauges{posGaugeIdx,6};
        end
        
        %Copy the data
        dataset.scalars.PLE_GAUGE_X.dat(index_proc) = gaugeX_callib * dataset.raw.gaugeX(index_raw);
        dataset.scalars.PLE_GAUGE_Y.dat(index_proc) = gaugeY_callib * dataset.raw.gaugeY(index_raw);
    end

    % add rest of scalars
    for i = 1:numel(scalarDevices)
        % extract scalar from the shot
        scalar = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataset.raw.settings{index_raw}, scalarDevices{i});
        scalarCalibrated = scalar * scalarCalibrations{i};
        dataset.scalars.(scalarVars{i}).dat(index_proc) = scalarCalibrated;
    end
    
    
    %% SCOPES
    % extract scopes
    for i = 1:numel(scopeDevices)
        
        % get the scope data structure
        scopeStruct = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataset.raw.BSA{index_raw}, scopeDevices{i});
        
        % save if the camera exists
        if isfield(scopeStruct,'value') || isfield(scopeStruct, 'samples')
            
            % check if one type of scope or the other
            if isfield(scopeStruct, 'value')
                voltages = double(scopeStruct.value);
                sensitivity = scopeStruct.sensitivity;
                offset = scopeStruct.offset;
                sampleInterval = scopeStruct.sampleInterval;
                firstSampleTime = scopeStruct.firstSampleTime;
            elseif isfield(scopeStruct, 'samples')
                voltages = double(scopeStruct.samples);
                sensitivity = 1;
                offset = 0;
                sampleInterval = 1;
                firstSampleTime = 0;
            end
            
            % voltage axis
            scopeVoltage = voltages*sensitivity + offset; % [V]
            scopeCalibrated = scopeVoltage * scopeCalibrations{i};
            dataset.scopes.(scopeVars{i}).dat{index_proc} = scopeCalibrated;
        
            % time axis
            samples = 0:(numel(voltages)-1);
            tAxis = samples*sampleInterval + firstSampleTime; % [ns]
            dataset.scopes.(scopeVars{i}).time_ns{index_proc} = tAxis;
            
        else
            % if it didn't work
            fprintf('Scope "%s" did not work in shot %i!\n', scopeDevices{i}, index_proc)
        end
    end
    % make empty struct if no scopes
    if ~isfield(dataset, 'scopes')
        dataset.scopes = struct();
    end
    
    
    %% CAMERAS
    % exctract cameras
    for i = 1:numel(camDevices)
        
        % extract image from shot
        syncOffset = camShotOffsets{i};
        imgStruct = matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataset.raw.BSA{index_raw + syncOffset}, camDevices{i});
        %disp(imgStruct)

        % save if the image exists
        if isstruct(imgStruct)

            % reshape the image
            if ~dataset.metadata.camTransposed{i}
                [xaxis, yaxis] = deal(imgStruct.imagePositionSet1, imgStruct.imagePositionSet2);
                [xsize, ysize] = deal(numel(xaxis), numel(yaxis));
                if ~dataset.metadata.camIsDigital{i}
                    %Analog cams return 1D array
                    image = reshape(imgStruct.imageSet, xsize, ysize)';
                else
                    image = imgStruct.image2D;
                end
            else
                [xaxis, yaxis] = deal(imgStruct.imagePositionSet2, imgStruct.imagePositionSet1);
                [xsize, ysize] = deal(numel(xaxis), numel(yaxis));
                if ~dataset.metadata.camIsDigital{i}
                    %Analog cams return 1D array
                    image = reshape(imgStruct.imageSet, ysize, xsize);
                else
                    image = imgStruct.image2D';
                end
            end
            
            if dataset.metadata.camFlipLR{i}
                image = fliplr(image);
            end
            if dataset.metadata.camFlipUD{i}
                image = flipud(image);
            end
            
            % cancel out the gain in the pixel count values
            if ~dataset.metadata.camIsDigital{i}
                switch imgStruct.videoGain
                    case 'X_ONE'
                        gain = 1;
                    case 'X_TWO_DOT_TWO'
                        gain = 2.2;
                    case 'X_SIX_DOT_FIVE'
                        gain = 6.5;
                    case 'X_EIGHT'
                        gain = 8;
                    otherwise
                        gain = 1;
                end
            else
                %Assumed for now,
                %gain doesn't seem to be returned in imgStruct
                gain = 1;
            end
            image = image/gain;
            dataset.images.(camVars{i}).dat{index_proc} = image;
            
            % save x-axis
            xcal = dataset.metadata.camCalibrationsX{i};
            if ~isnan(xcal)
                xaxis = xaxis * xcal;
                unitx = 'mm';
            else
                unitx = 'px';
            end
            dataset.images.(camVars{i}).xaxis = xaxis;
            dataset.images.(camVars{i}).unitx = unitx;
            
            % save y-axis
            ycal = dataset.metadata.camCalibrationsY{i};
            if ~isnan(ycal)
                yaxis = yaxis * ycal;
                unity = 'mm';
            else
                unity = 'px';
            end
            dataset.images.(camVars{i}).yaxis = yaxis;
            dataset.images.(camVars{i}).unity = unity;
            
        else
            % if it didn't work
            fprintf('Camera "%s" did not work in shot %i + %i!\n', camDevices{i}, index_proc, camShotOffsets{i})
        end
    end
    % make empty struct if no cams
    if ~isfield(dataset, 'images')
        dataset.images = struct();
    end
    
end
