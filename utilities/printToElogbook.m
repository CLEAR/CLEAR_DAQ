function [ elogEvent ] = printToElogbook( dataset )
    
    % create shortcut name for metadata
    MD = dataset.metadata;
    
    % overview and comment
    text = ['CLEAR DAQ: Dataset ' num2str(MD.dataset_id) ' (' MD.exptName ')' '\n'];
    for i = 1:size(MD.comment, 1)
        if numel(strtrim(MD.comment(i,:))) > 0
            text = [text '> ' MD.comment(i,:) '\n\n'];
        end
    end
    
    % shots, steps and progress
    if MD.isScan
        if ~MD.isScan2D
            text = [text 'Scan of "' MD.scanName '"' '\n'];
            text = [text '> ' num2str(MD.numSteps) ' steps (from ' num2str(MD.stepVals(1)) ' to ' num2str(MD.stepVals(end)) ')' '\n'];
        else
            text = [text '2D scan of "' MD.scanName '" (' num2str(MD.numSteps) ' steps) and "' MD.scanName2 '" (' num2str(MD.numSteps2) ' steps)' '\n'];
        end
        text = [text '> ' num2str(MD.shotsPerStep) ' shots per step' '\n'];
    else
        text = [text 'Simple DAQ' '\n'];
    end
    text = [text '> ' num2str(MD.numShots) ' of ' num2str(MD.numShotsRequested) ' shots collected (' num2str(floor(MD.duration/60)) ' min, ' num2str(floor(mod(MD.duration,60))) ' sec) : '];
    if MD.aborted
        text = [text 'ABORTED!' '\n'];
    else
        if MD.numShots == MD.numShots
            text = [text 'GREAT SUCCESS!' '\n'];
        else
            text = [text 'SOMETHING WENT WRONG!' '\n'];
        end
    end
    
    % data collected
    text = [text 'Data collected: ' '\n'];
    ncams = numel(MD.camVars);
    if ncams > 0
        if ncams==1
            text = [text '> ' num2str(ncams) ' camera ('];
        else
            text = [text '> ' num2str(ncams) ' cameras ('];
        end
        for i = 1:ncams
            text = [text MD.camVars{i}];
            if i < ncams
                text = [text ', '];
            end
        end
        text = [text ')' '\n'];
    end
    nscopes = numel(MD.scopeVars);
    if nscopes > 0
        if nscopes==1
            text = [text '> ' num2str(nscopes) ' scope ('];
        else
            text = [text '> ' num2str(nscopes) ' scopes ('];
        end
        for i = 1:nscopes
            text = [text MD.scopeVars{i}];
            if i < nscopes
                text = [text ', '];
            end
        end
        text = [text ')' '\n'];
    end
    
    % full path
    text = [text 'Path: ' MD.savepath '\n'];
    
    disp('ELOG functionality broken, was going to save text:')
    fprintf('\n\n')
    fprintf(text)
    fprintf('\n\n')
    return
    
    % create a new logbook event, adding text
    elogEvent = matlabeLogbookEvent('CLEAR', sprintf(text));
    
    % add a logbook key if it exists
    if isfield(MD, 'elogKey')
        changeEventTag(elogEvent, MD.elogKey)
    end
    
end

