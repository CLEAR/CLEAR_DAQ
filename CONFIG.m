function [ config ] = CONFIG()

    %% CLEAR_DAQ CONFIG

    % declare configuration struct
    config = struct();
    
    % GUI default values
    config.defaults.shotsPerStep = 5;
    config.defaults.numSteps = 11;
    config.defaults.saveBackground = false; % true|false
    config.defaults.printToElog = true; % true|false
    config.defaults.scanDirection = 'forward'; % forward|backward|both|random
    
    % info values (can be automated)
    %config.waitTimeTypical = 1.2; % [s]
    config.waitTimeTypical = 2.4; % [s]
    
    %Data basepath (system dependent)
    [hostRet, hostName] = system('hostname');
    if hostRet ~= 0
        error('Error in CONFIG(): Could not get hostname!');
    end
    if endsWith(strtrim(hostName), 'cern.ch')
        config.baseDataPath = '/clear/data/MatLab/Experiments/DAQ_datasets/';
    elseif  strcmp(strtrim(hostName), 'streifulv')
        config.baseDataPath = '/home/kyrsjo/DATA/DAQ_datasets/';
    end
    
    % cell grid of experiments and their options
    config.expts = { % {'experiment code', 'full description', 'dataset save path', 'elog tag', {default_cams}, {default_scopes} }
        'PLE',    'Plasma Lens Experiment',           [config.baseDataPath, 'PLE/'],       'PLASMALENS', {'DIS_CAP', 'PLE_OTR'}, {'DIS_CUR_IN', 'DIS_CUR_OUT', 'PMT_RAD', 'PHOTO_DIODE'};
        'WFM',    'Wakefield Monitor Experiment',     [config.baseDataPath, 'WFM/'],       'CLIC.WFM'  , {}, {};
        'OPER',   'CLEAR Operations',                 [config.baseDataPath, 'OPER/'],      'BEAMSETUP' , {}, {};
        'VESPER', 'Vesper',                           [config.baseDataPath, 'VESPER/'],    'VESPER'    , {}, {};
        'CBPM',   'Cavity BPM Experiment',            [config.baseDataPath, 'CBPM/'],      'CLIC.BPMS' , {}, {};
        'THZ',    'Terahertz Generation',             [config.baseDataPath, 'THZ/'],       'TERAHERTZ' , {}, {};
        'CHER',   'Cherenkov Diffraction Experiment', [config.baseDataPath, 'CHERENKOV/'], 'CHERENKOV' , {}, {};
    };
    config.datasetcounter_file = [config.baseDataPath, '/datasets.mat'];
    
    % cell grid of cameras
    config.cams = { % {'device name', 'description', 'dataset variable', 'sync offset', 'calib X [mm/px]', 'calib Y [mm/px]', 'transpose [true/false]', 'flipLR [true/false]', 'flipUD [true/false]', 'isDigital [true/false]'}
        %'CA.BTV0125/Image',                    'Virtual cathode',                  'VIR_CAT',                +1, NaN,     NaN,     false, false, true,  false;
        %'CA.BTV0215/Image',                    'Gun output',                       'GUN_OUT',                +1, NaN,     NaN,     false, false, true,  false;
        %'CA.BTV0390/Image',                    'Accelerator output',               'ACC_OUT',                +1, NaN,     NaN,     true,  true,  false, false;
        %'CAS.BTV0420/Image',                   'Upstream in-vac spectr.',          'UPS_SPC',                +1, NaN,     NaN,     false, false, true,  false;
        %'CAS.BTV0440/Image',                   'Vesper spectrometer',              'VSP_SPC',                +1, NaN,     NaN,     false, false, true,  false;
        'CA.BTV0620.DigiCam/ExtractionImage',  'Upstream screen 620',              'UP2_DIG',                 0, NaN,     NaN,     false, true,  false, true;
        %'CA.BTV0730/Image',                    'Upstream screen 730',              'UPS_OTR',                +1, 0.0210,  0.0240,  false, true,  false, false;
        'CA.BTV0730.DigiCam/ExtractionImage',  'Upstream screen 730',              'UPS_DIG',                 0, NaN,     NaN,     false, true,  false, true;
        %'CA.BTV0800/Image',                    'Discharge capillary',              'DIS_CAP',                +1, 0.0435,  0.0469,  false, false, false, false;
        'CA.BTV0800.DigiCam/ExtractionImage',  'Discharge capillary',              'DIS_DIG',                +2, NaN,     NaN,     false, false, false, true;
        %'CA.BTV0805/Image',                    'Electrode OTR',                    'ELT_OTR',                +1, 0.01081, 0.01081, false, false, true,  false;
        'CA.BTV0805.DigiCam/ExtractionImage',  'Electrode OTR',                    'ELT_DIG',                +2, 0.008611,0.008377, false, false, false, true; %Thursday 2024 beamtime, KNS 21/10/2024
        %'CA.BTV0810/Image',                    'Plasma lens screen',               'PLE_OTR',                +1, 0.0395,  0.0413,  false, false, false, false; %FLIP NOT CHECKED
        %%'CA.BTV0810.DigiCam/ExtractionImage',  'Plasma lens screen',               'PLE_DIG',                +2, 0.0269,  0.0269,  true,  false, false, true;
        'CA.BTV0810.DigiCam/ExtractionImage',  'Plasma lens screen',               'PLE_DIG',                +2, 0.02656,  0.02614,  true,  false, true, true;
        %'CA.BTV0910/Image',                    'Far field screen',                 'FAR_OTR',                +1, 0.0192,  0.0208,  false, true,  true,  false; % or (0.0153, 0.0167) before Mar 22
        'CA.BTV0910.DigiCam/ExtractionImage',  'Far field screen',                 'FAR_DIG',                +2, NaN,     NaN,     false, true,  false, true;
        %'CAS.BTV0930/Image',                   'Downstream spectrometer',          'DNS_SPC',                +1, 0.1524,  0.1524,  false, false, true,  false; % using a cross-to-cross distance 52 mm (to be scaled)
    };

    % cell grid of scopes
    config.scopes = { % {'device name', 'description', 'dataset variable', 'calibration (unit/V)', 'unit'}
        'CA.SCOPE11.CH02/Acquisition', 'Discharge current ingoing',   'DIS_CUR_IN',    463,      'A';
        'CA.SCOPE11.CH03/Acquisition', 'Discharge current outgoing',  'DIS_CUR_OUT',   447,      'A';
        'CA.SCOPE11.CH04/Acquisition', 'PMT radiation',               'PMT_RAD',       1,         'a.u.';
        %'CA.SCOPE11.CH04/Acquisition', 'Plasma light diode',          'PHOTO_DIODE',   1,         'a.u.';
        %'CA.PGLA-SA/Samples',           'Gun loop',                    'GUN_LOOP',      1,         'a.u.'; %NOTE OFFSET SHOULD BE -1
        %'CK.STPKI11A/Samples',         'Klystron 11 incident power',  'KLY11_PWR_IN',  1,         'a.u.';
        %'CK.STPKR11A/Samples',         'Klystron 11 reflected power', 'KLY11_PWR_REF', 1,         'a.u.';
        %'CK.STPKI15A/Samples',         'Klystron 15 incident power',  'KLY15_PWR_IN',  1,         'a.u.';
        %'CK.STPKR15A/Samples',         'Klystron 15 reflected power', 'KLY15_PWR_REF', 1,         'a.u.';
        %'CA.SCOPE04.CH02/Acquisition', 'Vesper spectrometer PMT',     'VESP_SPEC_PMT', 1,         'a.u.';
        'CE.SCOPE61.CH01/Acquisition', 'Gun beam charge monitor',     'GUN_BCM',       1.0,       'a.u.';
        'CE.SCOPE61.CH02/Acquisition', 'Vesper beam charge monitor',  'VESP_BCM',      1.0,       'a.u.';
        'CE.SCOPE61.CH03/Acquisition', 'THz beam charge monitor',     'THZ_BCM',       1.0,       'a.u.';

    };

    % all machine state variables to collect at the start of the dataset
    config.machineState = { % {'device name', 'description', 'dataset variable', 'calibration (multiplicative)', 'unit'}
        'CX.SSTREAK2-CTTIM/Delay#delay',         'Plasma lens coarse delay',                 'PLASMA_TIMING',     52,  'ns';
        'CL.ECLCNT4/CoarseDelay#delay',          'Discharge trigger delay',                  'DISCHARGE_TIMING',  1,   'ns';
        'CL.ECLCNT4/Enabled#enabled',            'Discharge trigger enabled',                'DISCHARGE_ENABLED', 1,   '';
        'CL.ECLCNT18/CoarseDelay#delay',         'Gated camera trigger delay',               'GATE_TIMING',       1,   'ns';
        'CL.ECLCNT18/Enabled#enabled',           'Gated camera trigger enabled',             'GATE_ENABLED',      1,   '';
        'CL.ECLCNT7/CoarseDelay#delay',          'Pulse picker opening time, coarse',        'PP_OPEN_COARSE',    1,   'ns';
        'CL.ECLCNT7/FineDelay#delay',            'Pulse picker opening time, fine',          'PP_OPEN_FINE',      1,   'ps';
        'CL.ECLCNT8/CoarseDelay#delay',          'Pulse picker closing time, coarse',        'PP_CLOSE_COARSE',   1,   'ns';
        'CL.ECLCNT8/FineDelay#delay',            'Pulse picker closing time, fine',          'PP_CLOSE_FINE',     1,   'ps';
        'CA.MOV0800-H/AQN#actualPosition',       'Plasma lens horizontal position',          'PL_MOVER_H',        1,   'steps';
        'CA.MOV0800-V/AQN#actualPosition',       'Plasma lens vertical position',            'PL_MOVER_V',        1,   'steps';
        'CA.QFD0350/Acquisition#currentAverage', 'VESPER triplet first quad (350)',          'VESPER_TRI_Q1',     1,   'A';
        'CA.QDD0355/Acquisition#currentAverage', 'VESPER triplet second quad (355)',         'VESPER_TRI_Q2',     1,   'A';
        'CA.QFD0360/Acquisition#currentAverage', 'VESPER triplet third quad (360)',          'VESPER_TRI_Q3',     1,   'A';
        'CA.QFD0510/Acquisition#currentAverage', 'CLIC module triplet first quad (510)',     'CLICMOD_TRI_Q1',    1,   'A';
        'CA.QDD0515/Acquisition#currentAverage', 'CLIC module triplet second quad (515)',    'CLICMOD_TRI_Q2',    1,   'A';
        'CA.QFD0520/Acquisition#currentAverage', 'CLIC module triplet third quad (520)',     'CLICMOD_TRI_Q3',    1,   'A';
        'CA.QFD0760/Acquisition#currentAverage', 'Plasma lens triplet first quad (760)',     'PL_TRI_Q1',         1,   'A';
        'CA.QDD0765/Acquisition#currentAverage', 'Plasma lens triplet second quad (765)',    'PL_TRI_Q2',         1,   'A';
        'CA.QFD0770/Acquisition#currentAverage', 'Plasma lens triplet third quad (770)',     'PL_TRI_Q3',         1,   'A';
        'CA.QDD0870/Acquisition#currentAverage', 'Downstream doublet first quad (870)',      'DN_DBL_Q1',         1,   'A';
        'CA.QFD0880/Acquisition#currentAverage', 'Downstream doublet second quad (880)',     'DN_DBL_Q2',         1,   'A';
        'CA.DHJ0710/Acquisition#currentAverage', 'Plasma lens kicker horizontal (710)',      'PL_KICK_H',         1,   'A';
        'CA.DVJ0710/Acquisition#currentAverage', 'Plasma lens kicker vertical (710)',        'PL_KICK_V',         1,   'A';
        'CA.DHJ0780/Acquisition#currentAverage', 'Upstream kicker horizontal (740)',         'UP_KICK_H',         1,   'A';
        'CA.DVJ0780/Acquisition#currentAverage', 'Upstream kicker vertical (740)',           'UP_KICK_V',         1,   'A';
        'CA.DHJ0840/Acquisition#currentAverage', 'Downstream kicker horizontal (840)',       'DN_KICK_N',         1,   'A';
        'CA.DVJ0840/Acquisition#currentAverage', 'Downstream kicker vertical (840)',         'DN_KICK_V',         1,   'A';
        'CA.BHB0400/Acquisition#currentAverage', 'VESPER spectrometer dipole current (400)', 'VESPER_DIPOLE',     1,   'A';
        'CA.BHB0900/Acquisition#currentAverage', 'Dump spectrometer dipole current (900)',   'DUMP_DIPOLE',       1,   'A';
        'CA.BCM01GAIN/RawValueSetting#rawValue', 'Beam charge monitor gain setting (raw)',   'BCM_GAIN',          1,   '';
        'CA.BCM01GAIN/Setting#enumValue',        'Beam charge monitor gain setting (named)', 'BCM_GAIN_NAMED',    1,   '';
    };

    % all beam synchronous variables to collect every shot
    config.scalars = { % {'device', 'description', 'dataset variable'}
        'CL.ECLCNT4/CoarseDelay#delay',          'Discharge trigger delay',                  'DISCHARGE_TIMING',  1,   'ns';
        'CL.ECLCNT4/Enabled#enabled',            'Discharge trigger enabled',                'DISCHARGE_ENABLED', 1,   '';
        'CA.MOV0800-H/AQN#actualPosition',       'Plasma lens horizontal position',          'PL_MOVER_H',        1,   'steps';
        'CA.MOV0800-V/AQN#actualPosition',       'Plasma lens vertical position',            'PL_MOVER_V',        1,   'steps';
        'CA.QFD0760/Acquisition#currentAverage', 'Plasma lens triplet first quad (760)',     'PL_TRI_Q1',         1,   'A';
        'CA.QDD0765/Acquisition#currentAverage', 'Plasma lens triplet second quad (765)',    'PL_TRI_Q2',         1,   'A';
        'CA.QFD0770/Acquisition#currentAverage', 'Plasma lens triplet third quad (770)',     'PL_TRI_Q3',         1,   'A';
        'CA.QDD0870/Acquisition#currentAverage', 'Downstream doublet first quad (870)',      'DN_DBL_Q1',         1,   'A';
        'CA.QFD0880/Acquisition#currentAverage', 'Downstream doublet second quad (880)',     'DN_DBL_Q2',         1,   'A';
    };
    
    % reference positions for the mover
    config.refPosH = 9547  ;
    config.refPosV =  18750;
    
    % cell grid of scans
    config.scans = { % {'name', '@scanfunction', 'unit', 'init value', 'final value', 'number of steps'}
        'Manual [paused]',                             @(x) questdlg(['Change parameter to:  ' num2str(x)],         'Manual paused scan',           'Okay, I changed it', 'Okay, I changed it'), '', 0, 10, 11;
        'Capillary pressure [manual]',                 @(x) questdlg(['Change pressure to:  '  num2str(x) ' mbar'], 'Manual pressure scan',         'Okay, I changed it', 'Okay, I changed it'), 'mbar', 2, 20, 7;
        'CMB charging voltage [manual]',               @(x) questdlg(['Change voltage to:  '   num2str(x) ' kV'],   'Manual charging voltage scan', 'Okay, I changed it', 'Okay, I changed it'), 'kV', 1.5, 2.4, 10;
        'Laser attentuator (charge)',                  @(x) setVariable( 'CTF2Motor2B', 'Setting', 'targetPosition', x, 1), 'steps', 0, 3000, 11;
        'Discharge fine timing',                       @(x) setVariable( 'CL.ECLCNT4',  'CoarseDelay', 'delay', int64(x)), 'ns', 11000, 12000, 21;
        %'Gated camera fine timing',                    @(x) setVariable( 'CL.ECLCNT18', 'CoarseDelay', 'delay', x), 'ns', 11000, 12000, 21;
        'Plasma lens mover horizontal [steps]',        @(x) setVariable( 'CA.MOV0800-H', 'CMD', 'requestedPosition', single(x), 1), 'steps', 72650, 74050, 31;
        'Plasma lens mover vertical [steps]',          @(x) setVariable( 'CA.MOV0800-V', 'CMD', 'requestedPosition', single(x), 1), 'steps', 192200, 195800, 31;
        %'Plasma lens mover horizontal [metric]',       @(x) moverHorMetricScanFunction(single(x)), 'um', -750, 750, 31;
        'Plasma lens mover horizontal (25 um step) [metric]', @(x) moverHorMetricScanFunction_25umstep(single(x), config.refPosH), 'um', -700, 700, 57;
        'Plasma lens mover horizontal (50 um step) [metric]', @(x) moverHorMetricScanFunction_50umstep(single(x), config.refPosH), 'um', -750, 750, 31;
        'Plasma lens mover vertical (10 um step) [metric]',  @(x) moverVertMetricScanFunction_10umstep(single(x), config.refPosV), 'um', -700, 700, 141;
        'Plasma lens mover vertical (25 um step) [metric]',  @(x) moverVertMetricScanFunction_25umstep(single(x), config.refPosV), 'um', -700, 700, 57;
        'Plasma lens mover vertical (50 um step) [metric]',  @(x) moverVertMetricScanFunction_50umstep(single(x), config.refPosV), 'um', -750, 750, 31;
        'Plasma lens in/out',                          @(x) setVariable( 'CA.MOV0800-H', 'CMD', 'requestedPosition', x, 1), 'steps', 66600, 73350, 2;
        'VESPER triplet first quad (350)',             @(x) setVariable( 'CA.QFD0350', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'VESPER triplet second quad (355)',            @(x) setVariable( 'CA.QDD0355', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'VESPER triplet third quad (360)',             @(x) setVariable( 'CA.QFD0360', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'CLIC module triplet first quad (510)',        @(x) setVariable( 'CA.QFD0510', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'CLIC module triplet second quad (515)',       @(x) setVariable( 'CA.QDD0515', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'CLIC module triplet third quad (520)',        @(x) setVariable( 'CA.QFD0520', 'SettingPPM', 'current', single(x), 1), 'A', 0,40,21;
        'Plasma lens triplet first quad (760)',        @(x) setVariable( 'CA.QFD0760', 'SettingPPM', 'current', single(x), 1), 'A', 0, 40, 21;
        'Plasma lens triplet second quad (765)',       @(x) setVariable( 'CA.QDD0765', 'SettingPPM', 'current', single(x), 1), 'A', 0, 40, 21;
        'Plasma lens triplet third quad (770)',        @(x) setVariable( 'CA.QFD0770', 'SettingPPM', 'current', single(x), 1), 'A', 0, 40, 21;
        'Emittance quad scan vertical (870)',          @(x) setVariable( 'CA.QDD0870', 'SettingPPM', 'current', single(x), 1), 'A', 80, 95, 15; % temp name
        'Emittance quad scan horizontal (880)',        @(x) setVariable( 'CA.QFD0880', 'SettingPPM', 'current', single(x), 1), 'A', 80, 95, 15; % temp name
        'Far upstream kicker (710) horizontal',        @(x) setVariable( 'CA.DHJ0710', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Far upstream kicker (710) vertical',          @(x) setVariable( 'CA.DVJ0710', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Upstream kicker (780) horizontal',            @(x) setVariable( 'CA.DHJ0780', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Upstream kicker (780) vertical',              @(x) setVariable( 'CA.DVJ0780', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Downstream kicker (840) horizontal',          @(x) setVariable( 'CA.DHJ0840', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Downstream kicker (840) vertical',            @(x) setVariable( 'CA.DVJ0840', 'SettingPPM', 'current', single(x), 1), 'A', -2, 2, 21;
        'Discharge on (odd) / off (even)',             @(x) setVariable( 'CL.ECLCNT1', 'Enabled',    'enabled', floor(mod(x,2)), 0), '', 1, 6, 6;
        'VESPER spectrometer dipole [current]',        @(x) setVariable( 'CA.BHB0400', 'SettingPPM', 'current', single(x), 1), 'A', 10, 60, 51;
        'VESPER spectrometer dipole [energy]',         @(x) setVariable( 'CA.BHB0400', 'SettingPPM', 'current', single(x)/3.9102, 1), 'MeV', 40, 200, 51;
        'Downstream spectrometer dipole [current]',    @(x) setVariable( 'CA.BHB0900', 'SettingPPM', 'current', single(x), 1), 'A', 10, 60, 51;
        'Pulse picker opening fine timing',            @(x) setVariable( 'CL.ECLCNT7', 'FineDelay', 'delay', single(x), 1), 'ps', 0, 5000, 21;
        'Pulse picker closing fine timing',            @(x) setVariable( 'CL.ECLCNT8', 'FineDelay', 'delay', single(x), 1), 'ps', 0, 5000, 21;
        'Capillary horizontal angle',                  @(x) horizontalAngleCorrectorScan(x), 'mrad', -2, 2, 7;
        'Capillary vertical angle',                    @(x) verticalAngleCorrectorScan(x), 'mrad', -2, 2, 7;
        'Deflecting cavity phase',                     @(x) setVariable( 'CK.LL-MKS31', 'Setting',   'PhaseSh_SP', single(x), 1), 'deg', 0, 360, 11;
    };

    % PositionGauge -- reading in same direction as the mover
    config.posgauges = { % name, address, xport, yport, xCal, yCal
        ...%'CLIC', 'clear-mitutoyo-clic-structure.cern.ch', 21, 22;
        'PLE',  'clear-mitutoyo-plasma-lens.cern.ch', 21, 22, 1.0, -1.0;
    };
end

